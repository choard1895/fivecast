#!/usr/local/bin/python3
from weather import Weather
from LPD8806 import LPD8806
from urllib import request # API fetching
from ast import literal_eval # JSON parsing 
from datetime import datetime # Date manipulation
from calendar import monthrange # For temp lerping
import time # For delay and current time
try:
    import RPi.GPIO as GPIO # Read from RasPi's I/O
except:
    print("ERROR: Cannot import RPi.GPIO, try running as superuser.")

# BEGIN USER CONFIG
current_url = 'http://api.openweathermap.org/data/2.5/weather?lat=42.3352&lon=-71.089396&units=metric'
hourly_url = 'http://api.openweathermap.org/data/2.5/forecast?lat=42.3352&lon=-71.089396&units=metric'
daily_url = 'http://api.openweathermap.org/data/2.5/forecast/daily?lat=42.3352&lon=-71.089396&units=metric&cnt=14'
monthly_avg = [-1.9, -0.9, 3.7, 8.9, 14.6, 19.8, 23.1, 22.2, 18.2, 12.7, 7.4, 0.9] # Boston, MA
button_channel = 7
# END USER CONFIG
    

num_modes = 5
mode_names = ["Sky", "Temperature", "Precipitation"]

# Fancy colors for *nix systems!
class termcolor:
    purple = '\033[35m'
    blue = '\033[34m'
    green = '\033[32m'
    yellow = '\033[33m'
    red = '\033[31m'
    white = '\033[0m'

def setup_button():
    # Set up the button as an input
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(button_channel, GPIO.IN)
    GPIO.add_event_detect(button_channel, GPIO.RISING, bouncetime=200)  

def process_button(current):
    # Increment the mode on rising edge
    # TODO: detect both rising and falling and switch by time
    # Short: inc mode
    # Med: brightness
    # Long: power
    if GPIO.event_detected(button_channel):
        print("[" + time.asctime() + "]" + termcolor.blue +
            "Selecting mode: ", (current + 1) % num_modes, termcolor.white)
        return (current + 1) % num_modes
    return current

def read_api(url):
    # Try to open the API URL. Abort on errors. 
    try:
        response = request.urlopen(url)
    except:
        raise ConnectionError("ERROR: Cannot open URL.")
        
    if (response.status != 200) or (response.reason != 'OK'):
        raise ConnectionError("ERROR: Cannot open URL. Status: " +
           str(response.status) + " Reason: " + response.reason)

    try:
        # Binary data is returned, process it as a UTF-8 string
        raw = str(response.read(), encoding='UTF-8')
        # JSON can be parsed directly to built-in types
        forecast = literal_eval(raw)
    except:
        raise RuntimeError("ERROR: Invalid data was received.")

    return forecast

def update_weather():
    # Attempt to update the weather from online, but be polite if it fails
    try:
        current = Weather.from_hourly(read_api(current_url))
        hourly = [Weather.from_hourly(x) for x in read_api(hourly_url)['list']]
        daily = [Weather.from_daily(x) for x in read_api(daily_url)['list']]

    except Exception as e:
        print("[" + time.asctime() + "]" + 
            termcolor.red + " Could not update weather.\n" + 
            termcolor.white + " - Attempt reported: \"" + str(e) + "\"")

    print("[" + time.asctime() + "]" + 
            termcolor.green + " Successfully fetched " +
            str(1 + len(hourly) + len(daily)) + " updates." + termcolor.white)

    return (current, hourly, daily)

def get_avg_temp(dt, lerp=True):
    # Get the average historic temperature given a timestamp
    # If lerp is true, adjacent months are averaged based on day
    date = datetime.fromtimestamp(dt)
    if not lerp:
        return monthly_avg[date.month - 1]
    # Using trapezoidal approximation, we assume the average temperature
    # during the month is close to the temperature half way through the 
    # month. For days before the half-point, we lerp backwards with the
    # previous month. For days after the half-point, we lerp forwards.
    length = monthrange(date.year, date.month)[1]
    alpha = ((date.day - 1) / (length - 1)) - 0.5
    if alpha < 0.0:
        return (1.0 + alpha) * monthly_avg[date.month - 1] - alpha * monthly_avg[(date.month - 2) % 12]
    else:
        return (1.0 - alpha) * monthly_avg[date.month - 1] + alpha * monthly_avg[date.month % 12]

def _temp_color(dev):
    low_scale = 10
    high_scale = 10
    if dev > 0:
        return (255,int(max(0,255-255*dev/high_scale)),int(max(0,255-255*1.6*dev/high_scale)))
    else:
        return (int(max(0,255+255*1.6*dev/low_scale)),int(max(0,255+255*dev/low_scale)), 255)


def _temp_dev():
    #update_weather()
    if len(hourly) < 1:
        print("sir, we've got a problem.")
    for w in hourly:
        avg = get_avg_temp(w.dt)
        print(w.temp - avg, w.temp, avg)

def main():
    # Initialize
    current = None
    hourly = []
    daily = []
    print("This is main.")

if __name__ == "__main__":
    main()
