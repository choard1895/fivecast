# LPD8806.py -- Control an LPD8806 RGB Strip with a RasPi
class LPD8806(object):
    """
    Simplifies interaction with a LPD8806 RGB LED strip connected to the RasPi's SPI port.
    Refer to http://learn.adafruit.com/system/assets/assets/000/001/589/medium800/diagram.png?1344878827
    for hookup information.
    """
    def __init__(self, segments, gamma=True, tint=True):
        """Create a new LED strip with a given number of segments"""
        super(LPD8806, self).__init__()
        if segments <= 0:
            raise ValueError("ERROR [LPD8806]: Invalid number of segments.")

        self.segments = segments

        # Open the Raspi's SPI device (must be sudo)
        try:
            self.dev = open("/dev/spidev0.0", "wb")
        except:
            print("FATAL ERROR [LPD8806]: Cannot open SPI device. Try running as superuser.")
            raise # Hard failure; not much we can do here

        # Create an 8-to-7 bit gamma lookup table
        self.gamma = bytearray(256)
        for i in range(256):
            self.gamma[i] = 0x80 | int(pow(float(i) / 255.0, 2.5) * 127.0 + 0.5)

        # Whitepoint compensation (experimental)
        # 1.0, 0.84, 0.71
        self.tint = (1.00, 0.92, 0.82)

        # Initialize local color array
        # The data format is a bit odd and requires an extra 0x00 every meter
        latch_bytes = int((self.segments + 31) / 32)
        self.buffer = bytearray(self.segments * 3 + 1 + latch_bytes)

        # Reset the strip, displaying twice just in case
        self.clear(True)
        self.show()

        self.do_gamma = gamma
        self.do_tint = tint

    def show(self):
        """Display the current buffer on the LED strip"""
        try:
            self.dev.write(self.buffer)
            self.dev.flush()
        except:
            print("FATAL ERORR [LPD8806]: could not write to device!")
            raise # Hard failure; not much we can do here

    def set_pixel(self, n, c):
        """
        Set the color of a specific segment number as an (r,g,b) triplet.
        Performs color-correction and gamma adjust if enabled.
        """
        if (n < 0) or (n >= self.segments):
            print("WARNING [LPD8806]: Invalid segment number")
            return # Soft failure; just ignore it

        # Clamp to [0-255]
        nc = tuple(max(0, min(x, 255)) for x in c)

        # Apply whitepoint compensation tint if enabled
        if self.do_tint:
            nc = tuple(int(self.tint[i] * nc[i]) for i in range(len(nc)))

        # Apply gamma curve or 8-to-7 decimate if disabled
        if self.do_gamma:
            nc = tuple(self.gamma[x] for x in nc)
        else:
            nc = tuple(0x80 | int(float(x) / 255.0 * 127.0 + 0.5) for x in nc)

        # Add into the buffer in GRB format
        self.buffer[n*3] = nc[1]
        self.buffer[n*3 + 1] = nc[0]
        self.buffer[n*3 + 2] = nc[2]

    def set_raw(self, n, c):
        """
        Set the raw value of a specific segment as an (r,g,b) triplet.
        Performs no color-correction nor gamma adjust
        Expects 7-bit rgb in range [128-255], as output from get_pixel
        """
        if (n < 0) or (n >= self.segments):
            print("WARNING [LPD8806]: Invalid segment number")
            return # Soft failure; just ignore it

        # Clamp to [128-255]
        nc = tuple(max(128, min(x, 255)) for x in c)

        # Add into the buffer in GRB format
        self.buffer[n*3] = nc[1]
        self.buffer[n*3 + 1] = nc[0]
        self.buffer[n*3 + 2] = nc[2]

    def set_list(self, a, display=False):
        """
        Set all pixels from a list of (r,g,b) triplets.
        List must be same length as LED strip.
        Optionally update the display with the new values.
        """
        if len(a) != self.segments:
            print("WARNING [LPD8806]: List length must equal number of segments")
            return # Soft failure; just ignore it

        # Set the pixel value of each item in the list
        for i in range(self.segments):
            self.set_pixel(i, a[i])

        if display:
            self.show()

    def get_pixel(self, n):
        """
        Return the color of a buffer segment as an (r,g,b) triplet.
        Note: Buffer state may not represent actual color display.
        Gamma and tint are not reversed; color will be 7-bit [128-255]
        """
        if (n < 0) or (n >= self.segments):
            print("WARNING [LPD8806]: Invalid segment number")
            return (0, 0, 0) # Soft failure; just ignore it

        # Grab the segment's data in the buffer
        grb = self.buffer[(n*3):(n*3 + 3)]

        # GRB -> RGB
        return (grb[1], grb[0], grb[2])

    def clear(self, display=False):
        """
        Clear the buffer, resetting to (0, 0, 0)
        Optionally update the display to turn off the LEDs.
        """
        for i in range(self.segments*3):
            self.buffer[i] = 0x80 # OFF

        if display:
            self.show()


    def enable_gamma(self, state):
        """Enable/Disable gamma correction (Enabled by default)"""
        self.do_gamma = state

    def enable_tint(self, state):
        """Enable/Disable whitepoint compensation (Enabled by default)"""
        self.do_tint = state

    def __str__(self):
        # TODO: add a nicer representation
        '''
        for i in range(0, self.segments * 3 - 2, 3):
            print(
                "r:",self.buffer[i+1],
                "g:",self.buffer[i],
                "b:",self.buffer[i+2])
        '''
        return __repr__(self)

    def __repr__(self):
        ret = "LPD8806(segments:" + str(self.segments)
        ret += ", buffer:["
        for i in range(0, self.segments * 3 - 2, 3):
            ret += str(int(i/3)) + ":(r:" + str(self.buffer[i+1])
            ret += ", g:" + str(self.buffer[i])
            ret += ", b:" + str(self.buffer[i+2]) + "), "
        ret += "(latching bytes)])"
        return ret
