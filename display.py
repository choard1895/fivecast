#!/usr/bin/python

# Light painting / POV demo for Raspberry Pi using
# Adafruit Digital Addressable RGB LED flex strip.
# ----> http://adafruit.com/products/306

import time

dev       = "/dev/spidev0.0"
spidev    = file(dev, "wb")

# Calculate gamma correction table.  This includes
# LPD8806-specific conversion (7-bit color w/high bit set).
print("Filling gamma array")
gamma = bytearray(256)
for i in range(256):
	gamma[i] = 0x80 | int(pow(float(i) / 255.0, 2.5) * 127.0 + 0.5)


pixels = [(0, 0, 0) for x in range(total_segs)]

raw = bytearray(total_segs * 3 + 1)

print("Converting...")

for x in range(total_segs):
	value = pixels[x]
	x3 = x * 3
	raw[x3]     = gamma[value[1]]
	raw[x3 + 1] = gamma[value[0]]
	raw[x3 + 2] = gamma[value[2]]


# Then it's a trivial matter of writing each column to the SPI port.
print "Displaying..."
spidev.write(raw)
spidev.flush()
time.sleep(0.1);		
