# Fivecast #
## Weather forecasting using a Raspberry Pi, Python, and LPD8806 RGB LED Strip ###

Weather constants are localized to Boston, MA, but can be easily changed.

RGB strip available from [Adafruit Industries](http://www.adafruit.com/products/306)

Must be run as a superuser!