# weather.py -- Store weather data

# API Weather codes
ICON2DESC = {'01n':'night/clear','02n':'night/few clouds',
'03n':'night/scattered clouds','04n':'night/broken clouds',
'09n':'night/shower rain','10n':'night/rain',
'11n':'night/thunderstorm','13n':'night/snow',
'50n':'night/mist','01d':'day/clear','02d':'day/few clouds',
'03d':'day/scattered clouds','04d':'day/broken clouds',
'09d':'day/shower rain','10d':'day/rain',
'11d':'day/thunderstorm','13d':'day/snow','50d':'day/mist'}

class Weather(object):
    """
    Stores a slice of weather data at a given point of time.
    Can read from both daily and hourly JSON formats returned by the API.
    """
    def __init__(self, dt, temp, rain, snow, icon):
        """
        Initialize weather with:
            dt -- Unix epoch time
            temp -- degrees Celsius
            rain -- mm/3h
            snow -- mm/3h
            icon -- API icon string
        """
        self.dt = dt
        self.temp = temp
        self.rain = rain
        self.snow = snow
        self.icon = icon

    @classmethod
    def from_hourly(cls, hourly_data):
        """Return new instance from JSON data in current/hourly API format"""
        if 'dt' not in hourly_data:
            raise KeyError("ERROR [Weather]: Key missing. Possibly corrupt data.")
            # Hard failure; catch the error in the main code
        return cls(hourly_data['dt'], 
            hourly_data['main']['temp'],
            hourly_data['rain']['3h'] if 'rain' in hourly_data else 0,
            hourly_data['snow']['3h'] if 'snow' in hourly_data else 0,
            hourly_data['weather'][0]['icon'])
    
    @classmethod
    def from_daily(cls, daily_data):
        """Return new instance from JSON data in daily API format"""
        if 'dt' not in daily_data:
            raise KeyError("ERROR [Weather]: Key missing. Possibly corrupt data.")
            # Hard failure; catch the error in the main code
        return cls(daily_data['dt'], 
            daily_data['temp']['day'],
            daily_data['rain']/8 if 'rain' in daily_data else 0,
            daily_data['snow']/8 if 'snow' in daily_data else 0,
            daily_data['weather'][0]['icon'])

    def icon_desc(self):
        """Return a plaintext description of weather icon"""
        return ICON2DESC[self.icon]

    def __str__(self):
        """Pretty-print data"""
        ret = "Weather at " + str(self.dt) + ":"
        ret += "\n\tTemp: " + str(self.temp) + "\xB0C"
        ret += "\n\tRain: " + str(self.rain) + " mm/3hr"
        ret += "\n\tSnow: " + str(self.snow) + " mm/3hr"
        ret += "\n\tIcon: " + str(self.icon) + " (" + self.icon_desc() + ")"
        return ret
    
    def __repr__(self):
        """Ugly-print data"""
        ret = "Weather("
        ret += "dt:" + str(self.dt) + ", "
        ret += "temp:" + str(self.temp) + ", "
        ret += "rain:" + str(self.rain) + ", "
        ret += "snow:" + str(self.snow) + ", "
        ret += "icon:" + str(self.icon) + ")"
        return ret

    def copy(self):
        """Duplicate data"""
        return Weather(self.dt, self.temp, self.rain, self.snow, self.icon)
