#!/usr/bin/python3
from weather import Weather
from urllib import request
from ast import literal_eval
import time
from datetime import datetime

'''
Modes:
Temperature
Precipitation
Sky
Dim
Off
Power Down
'''

# DEBUG
from graphics import *
win = GraphWin(512,512)
total_segs = 32
day_seg_num = 8
from pprint import pprint
class bcolors:
    purple = '\033[95m'
    blue = '\033[94m'
    green = '\033[92m'
    yellow = '\033[93m'
    red = '\033[91m'
    white = '\033[0m'
# END DEBUG

icon2color = {
'01d':(135, 206, 250), 
'02d':(143, 190, 219), 
'03d':(165, 196, 216), 
'04d':(183, 203, 216), 
'09d':(155, 165, 171), 
'10d':(179, 190, 198),
'11d':(192, 193, 179), 
'13d':(234, 234, 234), 
'50d':(141, 173, 192), 
'01n':(9, 30, 73),
'02n':(29, 45, 77),
'03n':(36, 49, 77),
'04n':(46, 56, 77),
'09n':(63, 68, 77),
'10n':(55, 63, 77),
'11n':(63, 68, 77),
'13n':(104, 108, 116),
'50n':(34, 57, 104)
}

monthly_avg = [-1.9, -0.9, 3.7, 8.9, 14.6, 19.8, 23.1, 22.2, 18.2, 12.7, 7.4, 0.9]


current_url = 'http://api.openweathermap.org/data/2.5/weather?lat=42.3352&lon=-71.089396&units=metric'
forecast_url = 'http://api.openweathermap.org/data/2.5/forecast?lat=42.3352&lon=-71.089396&units=metric'
daily_url = 'http://api.openweathermap.org/data/2.5/forecast/daily?lat=42.3352&lon=-71.089396&units=metric&cnt=14'


#current_url = 'http://api.openweathermap.org/data/2.5/weather?lat=34.206551&lon=-118.224685&units=metric'

#current = {'base': 'gdps stations', 'wind': {'speed': 1.48, 'deg': 228.5}, 'clouds': {'all': 0}, 'dt': 1376248188, 'main': {'temp_min': 24.44, 'pressure': 1015, 'temp': 25.97, 'humidity': 47, 'temp_max': 28}, 'coord': {'lat': 42.34, 'lon': -71.09}, 'weather': [{'main': 'Clear', 'description': 'Sky is Clear', 'icon': '01d', 'id': 800}], 'sys': {'country': 'US', 'sunset': 1376265031, 'sunrise': 1376214498}, 'cod': 200, 'name': 'Suffolk County', 'id': 4952349}
#forecast = {'list': [{'dt_txt': '2013-08-11 18:00:00', 'clouds': {'all': 0}, 'dt': 1376244000, 'main': {'grnd_level': 1026.29, 'sea_level': 1030.68, 'temp_min': 24.92, 'pressure': 1026.29, 'temp': 25.97, 'humidity': 52, 'temp_max': 25.97, 'temp_kf': 1.05}, 'weather': [{'main': 'Clear', 'description': 'sky is clear', 'icon': '01d', 'id': 800}], 'wind': {'speed': 1.48, 'deg': 228.5}, 'sys': {'pod': 'd'}}, {'dt_txt': '2013-08-11 21:00:00', 'clouds': {'all': 0}, 'dt': 1376254800, 'main': {'grnd_level': 1025.01, 'sea_level': 1029.43, 'temp_min': 25.76, 'pressure': 1025.01, 'temp': 26.76, 'humidity': 47, 'temp_max': 26.76, 'temp_kf': 1}, 'weather': [{'main': 'Clear', 'description': 'sky is clear', 'icon': '01d', 'id': 800}], 'wind': {'speed': 2.11, 'deg': 240.5}, 'sys': {'pod': 'd'}}, {'dt_txt': '2013-08-12 00:00:00', 'clouds': {'all': 0}, 'dt': 1376265600, 'main': {'grnd_level': 1024.75, 'sea_level': 1028.98, 'temp_min': 23.2, 'pressure': 1024.75, 'temp': 24.14, 'humidity': 50, 'temp_max': 24.14, 'temp_kf': 0.95}, 'weather': [{'main': 'Clear', 'description': 'sky is clear', 'icon': '01n', 'id': 800}], 'wind': {'speed': 2.63, 'deg': 258.001}, 'sys': {'pod': 'n'}}, {'dt_txt': '2013-08-12 03:00:00', 'clouds': {'all': 0}, 'dt': 1376276400, 'main': {'grnd_level': 1025.97, 'sea_level': 1030.21, 'temp_min': 20.23, 'pressure': 1025.97, 'temp': 21.12, 'humidity': 64, 'temp_max': 21.12, 'temp_kf': 0.89}, 'weather': [{'main': 'Clear', 'description': 'sky is clear', 'icon': '01n', 'id': 800}], 'wind': {'speed': 2.4, 'deg': 255.501}, 'sys': {'pod': 'n'}}, {'dt_txt': '2013-08-12 06:00:00', 'clouds': {'all': 0}, 'dt': 1376287200, 'main': {'grnd_level': 1025.34, 'sea_level': 1029.61, 'temp_min': 17.53, 'pressure': 1025.34, 'temp': 18.37, 'humidity': 79, 'temp_max': 18.37, 'temp_kf': 0.84}, 'weather': [{'main': 'Clear', 'description': 'sky is clear', 'icon': '01n', 'id': 800}], 'wind': {'speed': 1.72, 'deg': 241}, 'sys': {'pod': 'n'}}, {'dt_txt': '2013-08-12 09:00:00', 'clouds': {'all': 0}, 'dt': 1376298000, 'main': {'grnd_level': 1025.72, 'sea_level': 1029.8, 'temp_min': 16.11, 'pressure': 1025.72, 'temp': 16.9, 'humidity': 88, 'temp_max': 16.9, 'temp_kf': 0.79}, 'weather': [{'main': 'Clear', 'description': 'sky is clear', 'icon': '01n', 'id': 800}], 'wind': {'speed': 1.21, 'deg': 243.5}, 'sys': {'pod': 'n'}}, {'dt_txt': '2013-08-12 12:00:00', 'clouds': {'all': 0}, 'dt': 1376308800, 'main': {'grnd_level': 1026.03, 'sea_level': 1030.2, 'temp_min': 21.31, 'pressure': 1026.03, 'temp': 22.05, 'humidity': 67, 'temp_max': 22.05, 'temp_kf': 0.74}, 'weather': [{'main': 'Clear', 'description': 'sky is clear', 'icon': '01d', 'id': 800}], 'wind': {'speed': 1.55, 'deg': 248}, 'sys': {'pod': 'd'}}, {'dt_txt': '2013-08-12 15:00:00', 'clouds': {'all': 0}, 'dt': 1376319600, 'main': {'grnd_level': 1025.26, 'sea_level': 1029.47, 'temp_min': 25.17, 'pressure': 1025.26, 'temp': 25.85, 'humidity': 56, 'temp_max': 25.85, 'temp_kf': 0.68}, 'weather': [{'main': 'Clear', 'description': 'sky is clear', 'icon': '01d', 'id': 800}], 'wind': {'speed': 1.48, 'deg': 248.001}, 'sys': {'pod': 'd'}}, {'dt_txt': '2013-08-12 18:00:00', 'clouds': {'all': 12}, 'dt': 1376330400, 'main': {'grnd_level': 1023.96, 'sea_level': 1028.3, 'temp_min': 26.68, 'pressure': 1023.96, 'temp': 27.31, 'humidity': 51, 'temp_max': 27.31, 'temp_kf': 0.63}, 'weather': [{'main': 'Clouds', 'description': 'few clouds', 'icon': '02d', 'id': 801}], 'wind': {'speed': 2.28, 'deg': 245.5}, 'sys': {'pod': 'd'}}, {'dt_txt': '2013-08-12 21:00:00', 'clouds': {'all': 0}, 'dt': 1376341200, 'main': {'grnd_level': 1022.55, 'sea_level': 1026.93, 'temp_min': 26.86, 'pressure': 1022.55, 'temp': 27.44, 'humidity': 46, 'temp_max': 27.44, 'temp_kf': 0.58}, 'weather': [{'main': 'Clear', 'description': 'sky is clear', 'icon': '01d', 'id': 800}], 'wind': {'speed': 2.71, 'deg': 254}, 'sys': {'pod': 'd'}}, {'dt_txt': '2013-08-13 00:00:00', 'clouds': {'all': 0}, 'dt': 1376352000, 'main': {'grnd_level': 1022.53, 'sea_level': 1026.89, 'temp_min': 23.68, 'pressure': 1022.53, 'temp': 24.2, 'humidity': 50, 'temp_max': 24.2, 'temp_kf': 0.53}, 'weather': [{'main': 'Clear', 'description': 'sky is clear', 'icon': '01n', 'id': 800}], 'wind': {'speed': 2.56, 'deg': 256}, 'sys': {'pod': 'n'}}, {'dt_txt': '2013-08-13 03:00:00', 'clouds': {'all': 0}, 'dt': 1376362800, 'main': {'grnd_level': 1023.52, 'sea_level': 1027.65, 'temp_min': 20.15, 'pressure': 1023.52, 'temp': 20.62, 'humidity': 68, 'temp_max': 20.62, 'temp_kf': 0.47}, 'weather': [{'main': 'Clear', 'description': 'sky is clear', 'icon': '01n', 'id': 800}], 'wind': {'speed': 2.6, 'deg': 218.5}, 'sys': {'pod': 'n'}}, {'dt_txt': '2013-08-13 06:00:00', 'clouds': {'all': 36}, 'dt': 1376373600, 'main': {'grnd_level': 1023.17, 'sea_level': 1027.19, 'temp_min': 19.53, 'pressure': 1023.17, 'temp': 19.95, 'humidity': 84, 'temp_max': 19.95, 'temp_kf': 0.42}, 'weather': [{'main': 'Clouds', 'description': 'scattered clouds', 'icon': '03n', 'id': 802}], 'wind': {'speed': 2.75, 'deg': 217.501}, 'sys': {'pod': 'n'}}, {'dt_txt': '2013-08-13 09:00:00', 'clouds': {'all': 56}, 'dt': 1376384400, 'main': {'grnd_level': 1022.33, 'sea_level': 1026.44, 'temp_min': 19.21, 'pressure': 1022.33, 'temp': 19.58, 'humidity': 93, 'temp_max': 19.58, 'temp_kf': 0.37}, 'weather': [{'main': 'Clouds', 'description': 'broken clouds', 'icon': '04n', 'id': 803}], 'wind': {'speed': 2.76, 'deg': 212}, 'sys': {'pod': 'n'}}, {'dt_txt': '2013-08-13 12:00:00', 'clouds': {'all': 80}, 'dt': 1376395200, 'main': {'grnd_level': 1021.89, 'sea_level': 1025.81, 'temp_min': 21.04, 'pressure': 1021.89, 'temp': 21.36, 'humidity': 90, 'temp_max': 21.36, 'temp_kf': 0.32}, 'weather': [{'main': 'Clouds', 'description': 'broken clouds', 'icon': '04d', 'id': 803}], 'wind': {'speed': 2.65, 'deg': 196.001}, 'sys': {'pod': 'd'}}, {'dt_txt': '2013-08-13 15:00:00', 'clouds': {'all': 20}, 'dt': 1376406000, 'main': {'grnd_level': 1020.28, 'sea_level': 1024.45, 'temp_min': 27.09, 'pressure': 1020.28, 'temp': 27.35, 'humidity': 65, 'temp_max': 27.35, 'temp_kf': 0.26}, 'weather': [{'main': 'Clouds', 'description': 'few clouds', 'icon': '02d', 'id': 801}], 'wind': {'speed': 4.55, 'deg': 203.001}, 'sys': {'pod': 'd'}}, {'rain': {'3h': 2}, 'dt_txt': '2013-08-13 18:00:00', 'clouds': {'all': 68}, 'dt': 1376416800, 'main': {'grnd_level': 1018.52, 'sea_level': 1022.71, 'temp_min': 23.57, 'pressure': 1018.52, 'temp': 23.78, 'humidity': 92, 'temp_max': 23.78, 'temp_kf': 0.21}, 'weather': [{'main': 'Rain', 'description': 'light rain', 'icon': '10d', 'id': 500}], 'wind': {'speed': 3.56, 'deg': 229}, 'sys': {'pod': 'd'}}, {'rain': {'3h': 6}, 'dt_txt': '2013-08-13 21:00:00', 'clouds': {'all': 92}, 'dt': 1376427600, 'main': {'grnd_level': 1018.69, 'sea_level': 1022.59, 'temp_min': 20.22, 'pressure': 1018.69, 'temp': 20.38, 'humidity': 100, 'temp_max': 20.38, 'temp_kf': 0.16}, 'weather': [{'main': 'Rain', 'description': 'moderate rain', 'icon': '10d', 'id': 501}], 'wind': {'speed': 1.36, 'deg': 295.5}, 'sys': {'pod': 'd'}}, {'rain': {'3h': 2}, 'dt_txt': '2013-08-14 00:00:00', 'clouds': {'all': 92}, 'dt': 1376438400, 'main': {'grnd_level': 1018.34, 'sea_level': 1022.47, 'temp_min': 20.16, 'pressure': 1018.34, 'temp': 20.27, 'humidity': 100, 'temp_max': 20.27, 'temp_kf': 0.11}, 'weather': [{'main': 'Rain', 'description': 'light rain', 'icon': '10n', 'id': 500}], 'wind': {'speed': 1.76, 'deg': 256}, 'sys': {'pod': 'n'}}, {'rain': {'3h': 1}, 'dt_txt': '2013-08-14 03:00:00', 'clouds': {'all': 92}, 'dt': 1376449200, 'main': {'grnd_level': 1018.11, 'sea_level': 1022.23, 'temp_min': 19, 'pressure': 1018.11, 'temp': 19.05, 'humidity': 100, 'temp_max': 19.05, 'temp_kf': 0.05}, 'weather': [{'main': 'Rain', 'description': 'light rain', 'icon': '10n', 'id': 500}], 'wind': {'speed': 0.9, 'deg': 246}, 'sys': {'pod': 'n'}}, {'rain': {'3h': 1}, 'dt_txt': '2013-08-14 06:00:00', 'clouds': {'all': 92}, 'dt': 1376460000, 'main': {'grnd_level': 1017.2, 'sea_level': 1021.25, 'temp_min': 18.78, 'pressure': 1017.2, 'temp': 18.78, 'humidity': 100, 'temp_max': 18.78}, 'weather': [{'main': 'Rain', 'description': 'light rain', 'icon': '10n', 'id': 500}], 'wind': {'speed': 1.6, 'deg': 309.501}, 'sys': {'pod': 'n'}}, {'rain': {'3h': 0}, 'dt_txt': '2013-08-14 09:00:00', 'clouds': {'all': 100}, 'dt': 1376470800, 'main': {'grnd_level': 1017.67, 'sea_level': 1021.76, 'temp_min': 17.87, 'pressure': 1017.67, 'temp': 17.87, 'humidity': 100, 'temp_max': 17.87}, 'weather': [{'main': 'Clouds', 'description': 'overcast clouds', 'icon': '04n', 'id': 804}], 'wind': {'speed': 4.4, 'deg': 7.50031}, 'sys': {'pod': 'n'}}, {'rain': {'3h': 1}, 'dt_txt': '2013-08-14 12:00:00', 'clouds': {'all': 92}, 'dt': 1376481600, 'main': {'grnd_level': 1019.78, 'sea_level': 1023.85, 'temp_min': 16.84, 'pressure': 1019.78, 'temp': 16.84, 'humidity': 100, 'temp_max': 16.84}, 'weather': [{'main': 'Rain', 'description': 'light rain', 'icon': '10d', 'id': 500}], 'wind': {'speed': 3.81, 'deg': 3.50089}, 'sys': {'pod': 'd'}}, {'rain': {'3h': 0}, 'dt_txt': '2013-08-14 15:00:00', 'clouds': {'all': 44}, 'dt': 1376492400, 'main': {'grnd_level': 1021.52, 'sea_level': 1025.48, 'temp_min': 17.95, 'pressure': 1021.52, 'temp': 17.95, 'humidity': 92, 'temp_max': 17.95}, 'weather': [{'main': 'Clouds', 'description': 'scattered clouds', 'icon': '03d', 'id': 802}], 'wind': {'speed': 3, 'deg': 349.5}, 'sys': {'pod': 'd'}}, {'rain': {'3h': 0}, 'dt_txt': '2013-08-14 18:00:00', 'clouds': {'all': 0}, 'dt': 1376503200, 'main': {'grnd_level': 1021.9, 'sea_level': 1025.93, 'temp_min': 20.3, 'pressure': 1021.9, 'temp': 20.3, 'humidity': 74, 'temp_max': 20.3}, 'weather': [{'main': 'Clear', 'description': 'sky is clear', 'icon': '01d', 'id': 800}], 'wind': {'speed': 3.1, 'deg': 326}, 'sys': {'pod': 'd'}}, {'rain': {'3h': 0}, 'dt_txt': '2013-08-14 21:00:00', 'clouds': {'all': 0}, 'dt': 1376514000, 'main': {'grnd_level': 1022.18, 'sea_level': 1026.24, 'temp_min': 21.18, 'pressure': 1022.18, 'temp': 21.18, 'humidity': 58, 'temp_max': 21.18}, 'weather': [{'main': 'Clear', 'description': 'sky is clear', 'icon': '01d', 'id': 800}], 'wind': {'speed': 3.31, 'deg': 321.501}, 'sys': {'pod': 'd'}}, {'rain': {'3h': 0}, 'dt_txt': '2013-08-15 00:00:00', 'clouds': {'all': 0}, 'dt': 1376524800, 'main': {'grnd_level': 1023.54, 'sea_level': 1027.76, 'temp_min': 18.32, 'pressure': 1023.54, 'temp': 18.32, 'humidity': 63, 'temp_max': 18.32}, 'weather': [{'main': 'Clear', 'description': 'sky is clear', 'icon': '01n', 'id': 800}], 'wind': {'speed': 2.65, 'deg': 314.001}, 'sys': {'pod': 'n'}}, {'rain': {'3h': 0}, 'dt_txt': '2013-08-15 03:00:00', 'clouds': {'all': 0}, 'dt': 1376535600, 'main': {'grnd_level': 1025.46, 'sea_level': 1029.61, 'temp_min': 15.15, 'pressure': 1025.46, 'temp': 15.15, 'humidity': 77, 'temp_max': 15.15}, 'weather': [{'main': 'Clear', 'description': 'sky is clear', 'icon': '01n', 'id': 800}], 'wind': {'speed': 2.65, 'deg': 298.5}, 'sys': {'pod': 'n'}}, {'rain': {'3h': 0}, 'dt_txt': '2013-08-15 06:00:00', 'clouds': {'all': 0}, 'dt': 1376546400, 'main': {'grnd_level': 1026.04, 'sea_level': 1030.2, 'temp_min': 13.83, 'pressure': 1026.04, 'temp': 13.83, 'humidity': 83, 'temp_max': 13.83}, 'weather': [{'main': 'Clear', 'description': 'sky is clear', 'icon': '01n', 'id': 800}], 'wind': {'speed': 2.4, 'deg': 310.5}, 'sys': {'pod': 'n'}}, {'rain': {'3h': 0}, 'dt_txt': '2013-08-15 09:00:00', 'clouds': {'all': 0}, 'dt': 1376557200, 'main': {'grnd_level': 1026.49, 'sea_level': 1030.64, 'temp_min': 12.52, 'pressure': 1026.49, 'temp': 12.52, 'humidity': 92, 'temp_max': 12.52}, 'weather': [{'main': 'Clear', 'description': 'sky is clear', 'icon': '01n', 'id': 800}], 'wind': {'speed': 2.21, 'deg': 304}, 'sys': {'pod': 'n'}}, {'rain': {'3h': 0}, 'dt_txt': '2013-08-15 12:00:00', 'clouds': {'all': 0}, 'dt': 1376568000, 'main': {'grnd_level': 1027.8, 'sea_level': 1031.9, 'temp_min': 16.96, 'pressure': 1027.8, 'temp': 16.96, 'humidity': 71, 'temp_max': 16.96}, 'weather': [{'main': 'Clear', 'description': 'sky is clear', 'icon': '01d', 'id': 800}], 'wind': {'speed': 2.41, 'deg': 294}, 'sys': {'pod': 'd'}}, {'rain': {'3h': 0}, 'dt_txt': '2013-08-15 15:00:00', 'clouds': {'all': 0}, 'dt': 1376578800, 'main': {'grnd_level': 1027.88, 'sea_level': 1032, 'temp_min': 21.3, 'pressure': 1027.88, 'temp': 21.3, 'humidity': 61, 'temp_max': 21.3}, 'weather': [{'main': 'Clear', 'description': 'sky is clear', 'icon': '01d', 'id': 800}], 'wind': {'speed': 1.86, 'deg': 312}, 'sys': {'pod': 'd'}}, {'rain': {'3h': 0}, 'dt_txt': '2013-08-15 18:00:00', 'clouds': {'all': 20}, 'dt': 1376589600, 'main': {'grnd_level': 1027.49, 'sea_level': 1031.43, 'temp_min': 22.81, 'pressure': 1027.49, 'temp': 22.81, 'humidity': 59, 'temp_max': 22.81}, 'weather': [{'main': 'Clouds', 'description': 'few clouds', 'icon': '02d', 'id': 801}], 'wind': {'speed': 1.5, 'deg': 283.5}, 'sys': {'pod': 'd'}}, {'rain': {'3h': 0}, 'dt_txt': '2013-08-15 21:00:00', 'clouds': {'all': 0}, 'dt': 1376600400, 'main': {'grnd_level': 1026.37, 'sea_level': 1030.56, 'temp_min': 23.25, 'pressure': 1026.37, 'temp': 23.25, 'humidity': 55, 'temp_max': 23.25}, 'weather': [{'main': 'Clear', 'description': 'sky is clear', 'icon': '01d', 'id': 800}], 'wind': {'speed': 2.3, 'deg': 290}, 'sys': {'pod': 'd'}}, {'rain': {'3h': 0}, 'dt_txt': '2013-08-16 00:00:00', 'clouds': {'all': 20}, 'dt': 1376611200, 'main': {'grnd_level': 1027.19, 'sea_level': 1031.4, 'temp_min': 20.22, 'pressure': 1027.19, 'temp': 20.22, 'humidity': 58, 'temp_max': 20.22}, 'weather': [{'main': 'Clouds', 'description': 'few clouds', 'icon': '02n', 'id': 801}], 'wind': {'speed': 2.05, 'deg': 285.001}, 'sys': {'pod': 'n'}}, {'rain': {'3h': 0}, 'dt_txt': '2013-08-16 03:00:00', 'clouds': {'all': 0}, 'dt': 1376622000, 'main': {'grnd_level': 1028.84, 'sea_level': 1033, 'temp_min': 16.61, 'pressure': 1028.84, 'temp': 16.61, 'humidity': 75, 'temp_max': 16.61}, 'weather': [{'main': 'Clear', 'description': 'sky is clear', 'icon': '01n', 'id': 800}], 'wind': {'speed': 1.76, 'deg': 281.501}, 'sys': {'pod': 'n'}}, {'rain': {'3h': 0}, 'dt_txt': '2013-08-16 06:00:00', 'clouds': {'all': 0}, 'dt': 1376632800, 'main': {'grnd_level': 1029.14, 'sea_level': 1033.34, 'temp_min': 14.74, 'pressure': 1029.14, 'temp': 14.74, 'humidity': 82, 'temp_max': 14.74}, 'weather': [{'main': 'Clear', 'description': 'sky is clear', 'icon': '01n', 'id': 800}], 'wind': {'speed': 2.06, 'deg': 296.001}, 'sys': {'pod': 'n'}}, {'rain': {'3h': 0}, 'dt_txt': '2013-08-16 09:00:00', 'clouds': {'all': 0}, 'dt': 1376643600, 'main': {'grnd_level': 1029.61, 'sea_level': 1033.9, 'temp_min': 13.33, 'pressure': 1029.61, 'temp': 13.33, 'humidity': 91, 'temp_max': 13.33}, 'weather': [{'main': 'Clear', 'description': 'sky is clear', 'icon': '01n', 'id': 800}], 'wind': {'speed': 1.31, 'deg': 305.001}, 'sys': {'pod': 'n'}}, {'rain': {'3h': 0}, 'dt_txt': '2013-08-16 12:00:00', 'clouds': {'all': 0}, 'dt': 1376654400, 'main': {'grnd_level': 1031.26, 'sea_level': 1035.34, 'temp_min': 19.38, 'pressure': 1031.26, 'temp': 19.38, 'humidity': 73, 'temp_max': 19.38}, 'weather': [{'main': 'Clear', 'description': 'sky is clear', 'icon': '01d', 'id': 800}], 'wind': {'speed': 1.21, 'deg': 319.5}, 'sys': {'pod': 'd'}}, {'rain': {'3h': 0}, 'dt_txt': '2013-08-16 15:00:00', 'clouds': {'all': 0}, 'dt': 1376665200, 'main': {'grnd_level': 1031.72, 'sea_level': 1035.73, 'temp_min': 23.76, 'pressure': 1031.72, 'temp': 23.76, 'humidity': 60, 'temp_max': 23.76}, 'weather': [{'main': 'Clear', 'description': 'sky is clear', 'icon': '01d', 'id': 800}], 'wind': {'speed': 1.56, 'deg': 3.50006}, 'sys': {'pod': 'd'}}, {'rain': {'3h': 0}, 'dt_txt': '2013-08-16 18:00:00', 'clouds': {'all': 12}, 'dt': 1376676000, 'main': {'grnd_level': 1031.36, 'sea_level': 1035.43, 'temp_min': 24.58, 'pressure': 1031.36, 'temp': 24.58, 'humidity': 57, 'temp_max': 24.58}, 'weather': [{'main': 'Clouds', 'description': 'few clouds', 'icon': '02d', 'id': 801}], 'wind': {'speed': 1.66, 'deg': 76.0002}, 'sys': {'pod': 'd'}}], 'cnt': 41, 'cod': '200', 'message': 0.0101, 'city': {'country': 'US', 'coord': {'lat': 42.333431, 'lon': -71.082832}, 'name': 'Suffolk County', 'population': 0, 'id': 4952349}}

print("Fetching API")

current = literal_eval(str(request.urlopen(current_url).read(),encoding='UTF-8'))
hourly = literal_eval(str(request.urlopen(forecast_url).read(),encoding='UTF-8'))
daily = literal_eval(str(request.urlopen(daily_url).read(),encoding='UTF-8'))

print("Done.")

# add in error checking when incorporating into a function

#unix epoch time at the beginning of the day
n = datetime.now()
time_beginning = time.mktime(datetime( n.year, n.month, n.day).timetuple())

#current unix epoch time
time_now = time.time()

#current month
current_month = datetime.fromtimestamp(time_now).month

# [0,1) fraction of day completed
time_ratio_now = (time.time() - time_beginning) / 86400.0

# unix epoch times MAY BE FOR NEXT FEW DAYS
sunrise_time = current['sys']['sunrise']
sunset_time = current['sys']['sunset']

# fix day offsets
while ((sunrise_time - time_beginning) > 86400): sunrise_time -= 86400
while ((sunset_time - time_beginning) > 86400): sunset_time -= 86400


# shift to tomorrow if already occured
if (sunrise_time < time_now): sunrise_time += 86400
if (sunset_time < time_now): sunset_time += 86400

# seconds until next solar event
sunrise_in = sunrise_time - time_now
sunset_in = sunset_time - time_now

# age of current data in seconds
data_age = time_now - current['dt']

current_weather = Weather.from_hourly(current)
hourly_forecast = [Weather.from_hourly(x) for x in hourly['list']]
daily_forecast = [Weather.from_daily(x) for x in daily['list']]

# make sure current weather (not forecast[0]) is used for first segment! (though, they should be the same.)

print("Hours until next sunset:",sunset_in/60/60)
print("Hours until next sunrise:",sunrise_in/60/60)

if (sunrise_time < sunset_time):
    print("Sun is",bcolors.blue,"down")
else:
    print("Sun is",bcolors.yellow,"up")


p1 = Point(10,10)
p2 = Point(20,20)

for offset in range(0,8):
    seg_time = offset * 60*60*3
    r = Rectangle(p1,p2)
    if ((seg_time > sunset_in) ^ (seg_time > sunrise_in) ^ (sunset_in > sunrise_in)):
        #it's night
        state = bcolors.blue+'o'
        r.setFill('blue')
    else:
        #it's day
        state = bcolors.yellow+'*'
        r.setFill('yellow')
    r.draw(win)
    p1.x += 12
    p2.x += 12
    fac = (abs(min(sunset_in-seg_time,sunrise_in-seg_time)/(60*60*3/2)),abs(max(sunset_in-seg_time,sunrise_in-seg_time)/(60*60*3/2)))
    print(state,seg_time/60/60,fac[0]/(fac[0]+fac[1]),fac[1]/(fac[0]+fac[1]))
print(bcolors.white)

print("Hourly temperature")
print([w.temp for w in hourly_forecast])

print("Monthly avg at each hour")
print([monthly_avg[datetime.fromtimestamp(w.dt).month-1] for w in hourly_forecast])

print("Monthly Deviation")
print([w.temp - monthly_avg[datetime.fromtimestamp(w.dt).month-1] for w in hourly_forecast])

print("Daily temperature")
print([w.temp for w in daily_forecast])

print("Monthly avg at each day")
print([monthly_avg[datetime.fromtimestamp(w.dt).month-1] for w in daily_forecast])

print("Monthly Deviation")
print([w.temp - monthly_avg[datetime.fromtimestamp(w.dt).month-1] for w in daily_forecast])

def temp_color(dev):
    low_scale = 10
    high_scale = 10
    if dev > 0:
        return (255,int(max(0,255-255*dev/high_scale)),int(max(0,255-255*1.6*dev/high_scale)))
    else:
        return (int(max(0,255+255*1.6*dev/low_scale)),int(max(0,255+255*dev/low_scale)), 255)

p1 = Point(10,22)
p2 = Point(20,32)
p3 = Point(10,34)
p4 = Point(20,44)

# (0,0,255) - (255,255,0) - (255,0,0)
# (0,0,255) - (255,255,255) - (255,0,0)
# (0,0,255) - (0,0,0) - (255,0,0)

for w in hourly_forecast:
    c=temp_color(w.temp-monthly_avg[datetime.fromtimestamp(w.dt).month-1])
    s = Rectangle(p1,p2)
    s.setFill(color_rgb(c[0],c[1],c[2]))
    s.draw(win)
    p1.x += 12
    p2.x += 12

for w in daily_forecast:
    c=temp_color(w.temp-monthly_avg[datetime.fromtimestamp(w.dt).month-1])
    s = Rectangle(p3,p4)
    s.setFill(color_rgb(c[0],c[1],c[2]))
    s.draw(win)
    p3.x += 12
    p4.x += 12

while True:
    temp=0


'''

Light rain — when the precipitation rate is < 2.5 millimetres (0.098 in) per hour
Moderate rain — when the precipitation rate is between 2.5 millimetres (0.098 in) - 7.6 millimetres (0.30 in) or 10 millimetres (0.39 in) per hour[98][99]
Heavy rain — when the precipitation rate is > 7.6 millimetres (0.30 in) per hour,[98] or between 10 millimetres (0.39 in) and 50 millimetres (2.0 in) per hour[99]
Violent rain — when the precipitation rate is > 50 millimetres (2.0 in) per hour[99]

Light		< 7.5 mm/3h
Moderate	< 22.5 mm/3h
Heavy		< 150 mm/3h
Violent 	> 150 mm/3h



Rainfall (average in inches)
Jan	Feb	Mar	Apr	May	June	July	Aug	Sept	Oct	Nov	Dec
3.62	3.38	3.86	3.61	3.22	3.15	3.15	3.60	3.19	3.29	3.91	3.65


Snowfall (average in inches)
Jan	Feb	Mar	Apr	May	June	July	Aug	Sept	Oct	Nov	Dec
12.0	11.3	7.9	0.9	0	0	0	0	0	0	1.3	7.5

27.5844	25.7556	29.4132	27.5082	24.5364	24.003	24.003	27.432	24.3078	25.0698	29.7942	27.813
91.44	86.106	60.198	6.858	0	0	0	0	0	0	9.906	57.15


0.03655832191781


Days	 	Inches	Centimetres
  6.7	January	12.9	  32.8
  5.3	February	10.9	  27.7
  4.2	March	  7.8	  19.8
  0.7	April	  1.9	    4.8
  0.8	November	  1.3	    3.3
  4.6	December	  9.0	  22.9
22.4	Year	43.8	111.3


http://bugs.openweathermap.org/projects/api/wiki/Api_2_5_history
http://bugs.openweathermap.org/projects/api/wiki/Weather_Condition_Codes
http://bugs.openweathermap.org/projects/api/wiki/Weather_Data
http://openweathermap.org/api

blue = cold, yellow = nice, red = hot
blue-brightness for rain, white for snow, yellow for thunde



Fetch icons at http://openweathermap.org/img/w/10d.png


id  City identificator  mandatory
dt   Time of data receiving in unixtime GMT  mandatory
coord.lat coord.lng  City location   mandatory
name     City name   mandatory
main.temp    Temperature in Kelvin. Subtracted 273.15 from this figure to convert to Celsius.    mandatory
main.humidity    Humidity in %   mandatory
main.temp_min main.temp_max  Minimum and maximum temperature     Optional
main.pressure    Atmospheric pressure in hPa     mandatory
wind.speed   Wind speed in mps   mandatory
wind.deg     Wind direction in degrees (meteorological)  mandatory
clouds.all   Cloudiness in %     mandatory
weather  weather condition More about data Weather Condition Codes   Optional
rain.3h  Precipitation volume mm per 3 hours     Optional
snow.3h  Precipitation volume mm per 3 hours     Optional


'''
